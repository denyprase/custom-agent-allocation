require "faraday"

$queue = []

class ApplicationController < ActionController::API
    BASE_URL = 'https://multichannel.qiscus.com'
    APP_ID = "karm-gzu41e4e4dv9fu3f"
    APP_SECRET = "9d54f6697777234479341ed20dba7517"

    def index
        render plain: "OK"
    end

    def allocate
        data = params[:application]
        if data == nil
            return render json: {message: "empty"}
        end
        room_id = data[:room_id]
        email = data[:email]
        if is_chat_event(data)
            message = {room_id: room_id, email: email}
            if $queue.include? message
                return render json: {message: "OK"}
            end
            agent = get_available_agent
            if agent != nil
                response = assign_agent(room_id, agent[:id])
                return render json: response
            end
            $queue.unshift message
            return render json: {message: "added to queue"}
        end
        if is_resolved_event(data)
            if $queue.length > 0
                message = $queue.pop
                agent = get_available_agent
                if agent != nil
                    response = assign_agent(message[:room_id], agent[:id])
                    return render json: response
                end
            end    
            return render json: {message: "OK"}
        end
    end

    private
    def is_chat_event(payload)
        chat_keys = [
            'app_id',
            'avatar_url',
            'candidate_agent',
            'email',
            'extras',
            'is_new_session',
            'is_resolved',
            'latest_service',
            'name',
            'room_id',
            'source'
        ]
        params_keys = payload.keys
        return Set.new(params_keys) == Set.new(chat_keys)
    end

    def is_resolved_event(payload)
        resolve_keys = ['customer', 'resolved_by', 'service']
        params_keys = payload.keys
        return Set.new(params_keys) == Set.new(resolve_keys)
    end

    def get_agent_by_ids(agent_ids)
        url = "#{BASE_URL}/api/v1/admin/agents/get_by_ids"
        id_params = {ids: agent_ids}
        headers = {
            "Content-Type" => "application/json",
            "Qiscus-App-Id" => APP_ID,
            "Qiscus-Secret-Key" => APP_SECRET
        }
        response = Faraday.get(url, params=id_params, headers=headers)
        # puts response.env.url.to_s
        # puts response.status
        return response.body
    end

    def is_agent_available(agent_data)
        available = agent_data[:is_available]
        count = agent_data[:current_customer_count]
        if (available != nil) && (count != nil)
            if available && (count < 2)
                return true
            end
        end
        return false
    end
    
    def get_available_agent
        agent_ids = [152425, 152426, 152428]
        result = get_agent_by_ids(agent_ids)
        parsed = JSON.parse(result, symbolize_names: true)
        available_agents = parsed[:data].select {|data| is_agent_available(data)}
        return available_agents[0]
    end

    def assign_agent(room_id, agent_id)
        url = "#{BASE_URL}/api/v1/admin/service/assign_agent"
        payload = {
            room_id: room_id,
            agent_id: agent_id,
            max_agent: 2
        }
        headers = {
            "Content-Type" => "application/x-www-form-urlencoded",
            "Qiscus-App-Id" => APP_ID,
            "Qiscus-Secret-Key" => APP_SECRET
        }
        response = Faraday.post(url, body=payload, headers=headers)
        # puts response.status
        # puts response.body
        return response.body
    end

end
